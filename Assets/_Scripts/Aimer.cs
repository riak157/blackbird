﻿using UnityEngine;
using System.Collections;

public class Aimer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	/*
	 * These Two methods are general purpose for angling the current transform
	 * at an object or a direction. Note: Due to how the sprites are only
	 * rendered in the X Y plane, an extra multiplication is done to make the 
	 * standard for "facing" be the right transform vector as opposed to the
	 * forward transform vector. 
	 */
	public void PointAtTarget(GameObject targetLook, float speed){
		Vector3 lookPos = targetLook.transform.position - transform.position;
		
		//lookPos.y = 0;
		var rotation = Quaternion.LookRotation(lookPos);
		rotation *= Quaternion.Euler(0, -90, 0);
		
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed);
	}

	public void PointAtDirection (Vector3 direction, float speed) {
		var rotation = Quaternion.LookRotation(direction);
		rotation *= Quaternion.Euler(0, -90, 0);
		
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed);

	}

}
