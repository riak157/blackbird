﻿using UnityEngine;
using System.Collections;

public class FrictionlessObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		collider.material.staticFriction = 0;
		collider.material.dynamicFriction = 0;

	}
	

}
