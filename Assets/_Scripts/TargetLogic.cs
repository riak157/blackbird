﻿using UnityEngine;
using System.Collections;

public class TargetLogic : MonoBehaviour {
	private SpriteRenderer sprite_renderer;
	// Use this for initialization
	void Start () {
		sprite_renderer = transform.FindChild("TargetGraphic").GetComponent<SpriteRenderer> ();
		sprite_renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {


	
	}

	public void EnableTarget(){

		sprite_renderer.enabled = true;
		}


	public void DisableTarget(){

		sprite_renderer.enabled = false;
	}
}
