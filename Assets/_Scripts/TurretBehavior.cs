﻿using UnityEngine;
using System.Collections;

public class TurretBehavior : BitPlayer {

	private Aimer barrelPosition;
	private float barrelSpeed = 5.0f;
	private TurretShot fireController;
	// Use this for initialization
	new void Start () {
				base.Start ();
				Transform detection = transform.FindChild ("DetectionSphere");
				detection.GetComponent<DetectionBehavior> ().RegisterDetector (this);
				Transform barrel = transform.FindChild ("TurretBarrelRotation");

				barrelPosition = barrel.GetComponent<Aimer> ();

				Transform fireControl = barrel.FindChild ("TurretBarrel");
				fireController = fireControl.GetComponent<TurretShot> ();


		}

	void FixedUpdate (){
	

		if (target.Count > 0) {
					if(fireController.GetFiringState() == false){
						fireController.EnableFire();
						
					}


						barrelPosition.PointAtTarget ((GameObject)target [0], barrelSpeed * Time.fixedDeltaTime);

				} else {

			if(fireController.GetFiringState() == true){
				fireController.DisableFire();
				
			}

			barrelPosition.PointAtDirection (transform.right * -1.0f, barrelSpeed * Time.fixedDeltaTime);
				}

		
	}
	
}
