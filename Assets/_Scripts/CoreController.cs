﻿using UnityEngine;
using System.Collections;

public class CoreController : MonoBehaviour {

	public float speed;
	public float qbSpeed;
	public float groundImpulse;
	public float maxWalkingSpeed;
	public float thrust;
	public float health = 500;
	public float maxHealth = 500;

	public float normalBoostCooldown;
	public float quickBoostCooldown;

	private float qbTime;
	private float releaseTime = -1.0f;
	private float jumpInterval = 1.0f;
	private float distToGround;
	public GameObject weapon1;
	public GameObject weapon2;
	public GameObject FCS;

	private bool glideMode;
	public bool facingRight = true;


	private ArrayList target;


	public void AddTarget(GameObject go){
		if (!target.Contains (go)) {
						target.Add (go);
				}
	}

	public bool IsGrounded(){

		return Physics.Raycast (transform.position, -transform.up, (distToGround + 0.05f));
	}

	public bool IsStationary(float mh, float mv){
		if (Mathf.Abs(mh) > 0.1f || Mathf.Abs (mv) > 0.1f) {
			//Debug.Log (("X: " + moveHorizontal + " Z: " + moveVertical));
			return false;
		} else {
			return true;
		}

		}

	public void RemoveTarget (GameObject go){
		//Debug.Log ("Target Removed " + go.name);
		target.Remove (go);
		}

	// Use this for initialization
	void Start () {
		distToGround = collider.bounds.extents.y;
		target = new ArrayList ();
		qbTime = -quickBoostCooldown;
		rigidbody.maxAngularVelocity = 0;
		collider.material.staticFriction = 0;

		collider.material.dynamicFriction = 0.8f;
		glideMode = false;
	}

	void PointAtTarget(GameObject wep, GameObject targetLook){
		Vector3 lookPos = getDirection(wep.transform.position, targetLook);
		
		//lookPos.y = 0;
		var rotation = Quaternion.LookRotation(lookPos);
		
		rotation *= Quaternion.Euler(0, -90, 0); // this add a 90 degrees Y rotation Making "Right" forward
		
		wep.transform.rotation = Quaternion.Slerp(wep.transform.rotation, rotation, 10);
		}


	void ResetRotation(GameObject wep){
			if (facingRight) {
						wep.transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
				} else {
					wep.transform.rotation = Quaternion.Euler (0.0f, -180.0f, 0.0f);
				}
		}
	// UpdFate is called once per frame
	void Update () {
		if (facingRight) {
						transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
				} else {
						transform.rotation = Quaternion.Euler (0.0f, -180.0f, 0.0f);

				}
		if (target.Count == 0) {
			ResetRotation(weapon1);
			ResetRotation(weapon2);

				} else {
			GameObject nextTarget = (GameObject) target[0];
			GameObject nextnextTarget = nextTarget;
			if(target.Count > 1){
				nextnextTarget = (GameObject) target[1];
			}

			//Checks if target is unexepectedly Destroyed
			if(nextnextTarget != null){
				PointAtTarget(weapon2, nextnextTarget);
			}
			else if(target.Count > 1){
				target.RemoveAt(1);
				
			}

			if(nextTarget != null){
				PointAtTarget(weapon1, nextTarget);
			}else{
				target.RemoveAt(0);

			}



			//weapon1.transform.LookAt(nextTarget.transform.position);
				}
	}

	void FixedUpdate(){
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		bool quickboost = Input.GetButtonDown ("Boost");
		bool boostRelease = Input.GetKeyUp ("space");

		bool changeFacing = Input.GetKeyDown ("e");


		bool moveFlight =false;
		bool isBoosting = Input.GetKey ("space");
		float maxVelocityScalerX = 1.0f;
		float maxVelocityScalerZ = 1.0f;

		if (changeFacing) {
			facingRight = !facingRight;
			for(int i = 0; i < target.Count ; i++){
				GameObject toClear = (GameObject) target[i];
				toClear.GetComponent<TargetLogic>().DisableTarget();
			}
			target.Clear();
			FCS.GetComponent<FCSBehavior>().Refresh();

				}

		Vector3 movementInput = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		movementInput.Normalize ();
		Vector3 movement = movementInput;
		Vector3 upMovement = new Vector3 ();
		//Cap force used for ground movement

//
//		if (boostStart) {
//			if(!moveFlight && Time.time - releaseTime < jumpInterval){
//				
//				moveFlight = true;
//			}
//				}

		if (boostRelease) {
			releaseTime = Time.time;
			//Debug.Log("Boost released at: " + releaseTime);
				}


		if (isBoosting) {
			if(Time.time - qbTime >= normalBoostCooldown){
				rigidbody.AddForce (movement * speed * Time.deltaTime);
			}
//			if(IsStationary(moveHorizontal,moveVertical)){
//				moveFlight = true;
//			}
//
//			if(!IsGrounded()){
//				moveFlight = true;
//			}

			if(!IsStationary (moveHorizontal,moveVertical)&&IsGrounded() && (Time.time - releaseTime) > jumpInterval){
				glideMode = true;
			}


			if(!glideMode){
				moveFlight = true;

			}

				} else {
						float xcap = (rigidbody.velocity.x / maxWalkingSpeed) * moveHorizontal;
						if (xcap > 0) {
								maxVelocityScalerX = Mathf.Clamp (1.0f - xcap, 0.0f, 1.0f);
								movement.x *= maxVelocityScalerX;
						}

						float zcap = (rigidbody.velocity.z / maxWalkingSpeed) * moveVertical;
						if (zcap > 0) {
								maxVelocityScalerZ = Mathf.Clamp (1.0f - zcap, 0.0f, 1.0f);
								movement.z *= maxVelocityScalerZ;
						}
						//rigidbody.AddForce (movement * maxWalkingSpeed, ForceMode.VelocityChange);

						/*


		*/
						glideMode = false;
						rigidbody.AddForce (movement * groundImpulse * Time.deltaTime, ForceMode.VelocityChange);
				}

		if (moveFlight) {
			//	Vector3 flightforce = new Vector3(0.0f, thrust*Time.deltaTime,0.0f);
			upMovement.y += thrust*Time.deltaTime;
			rigidbody.AddForce (upMovement);
			
		}

		if (quickboost && (Time.time - qbTime >= quickBoostCooldown)) {
			//Debug.Log("quickboostin");
			qbTime = Time.time;
			rigidbody.AddForce(movementInput*qbSpeed, ForceMode.Impulse);
				}
			
	}

	Vector3 getDirection(Vector3 position, GameObject target){
		return target.transform.position - position;
	}
}
