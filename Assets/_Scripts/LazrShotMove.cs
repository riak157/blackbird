﻿using UnityEngine;
using System.Collections;

public class LazrShotMove : Projectile {

	public float speed;
	public GameObject hitExplode;
	private Vector3 contactPosition;
	// Use this for initialization
	void Start () {
		rigidbody.velocity = transform.right * speed;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Explode(Vector3 pos){

		Instantiate (hitExplode,  pos, transform.rotation);
		Destroy (gameObject);
	}



	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag != "Boundary") {
			Explode (other.contacts[0].point);
			Debug.Log ("Collided with " + other.gameObject);
		}
	}

}
