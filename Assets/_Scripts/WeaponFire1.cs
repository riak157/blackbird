﻿using UnityEngine;
using System.Collections;

public class WeaponFire1 : MonoBehaviour {

	public GameObject lazrShot;
	public Transform shotSpawn;
	public CoreController core;


	public float fireRate;

	private float nextFire;
	// Use this for initialization
	void Start () {
		core = GetComponentInParent<CoreController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			GameObject shot = (GameObject) Instantiate(lazrShot, shotSpawn.position, shotSpawn.rotation);
			shot.GetComponent<Projectile>().SetCore(core);
		}
	}
}
