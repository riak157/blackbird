﻿using UnityEngine;
using System.Collections;

public class TurretShot : MonoBehaviour {


	public float shotDelay;
	public GameObject turretBullet;
	private float lastshot = 0.0f;
	private Transform shotSpawn;
	private bool firingEnabled = false;
	// Use this for initialization
	void Start () {
	
		shotSpawn = transform.FindChild ("TurretShotSpawn");
	}
	
	// Update is called once per frame
	void Update () {
	
		if (firingEnabled && Time.time - lastshot > shotDelay) {
			Instantiate(turretBullet, shotSpawn.position, shotSpawn.rotation);
			lastshot = Time.time;
				}
	}

	public bool GetFiringState(){

		return firingEnabled;
		}

	public void EnableFire(){

		firingEnabled = true;
	}

	public void DisableFire(){
		firingEnabled = false;

	}
}
