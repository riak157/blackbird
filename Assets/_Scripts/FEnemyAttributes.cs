﻿using UnityEngine;
using System.Collections;

public class FEnemyAttributes : BitPlayer {




	public bool facingLeft = true;

	public float speed;
	public float shotStagger;
	public float collisionStagger;

	private float staggerTill = 0.0f;
	// Use this for initialization

	void MoveForward(){
		rigidbody.velocity = transform.right * -(speed);
		}

	void StopMovement(){
		rigidbody.velocity = transform.right * 0.0f;

		}

	new void Start () {
		base.Start ();
	}


	new void OnCollisionEnter(Collision collision){
		base.OnCollisionEnter (collision);
		Debug.Log("PlayerCollision: " + collision.collider.tag);
		if (collision.collider.tag == "Core") {
			staggerTill = Time.time + collisionStagger;
			Debug.Log("PlayerCollision");
		}

		}


	void OnTriggerEnter(Collider other){
					
				if (other.tag == "EnemyCatcher") {

			Destroy(gameObject);

				}



			if (other.tag == "Shot") {
			staggerTill = Time.time + shotStagger;
			StopMovement();
			health = health - 10.0f;
			if(health < 0.0f){
				destructionflag = true;
				Projectile shot = other.gameObject.GetComponent<Projectile>();
				shot.getCore().RemoveTarget(targetCenter); //Unsure if this actually works
			}
			Destroy (other.gameObject);
				}

			
		}
	// Update is called once per frame
	void Update () {
		//Debug.Log ("Is sleeping: " + rigidbody.IsSleeping ());
		if (facingLeft) {
			transform.rotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
		} else {
			transform.rotation = Quaternion.Euler (0.0f, -180.0f, 0.0f);
			
		}
		if (destructionflag) {
			Destroy (gameObject);
			Instantiate(explosion,transform.position, transform.rotation);
				}
	}

	void FixedUpdate(){
		if (Time.time > staggerTill) {
			MoveForward();

				}

	}


}
