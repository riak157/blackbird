﻿using UnityEngine;
using System.Collections;
using Vectrosity;


public class FCSBehavior : MonoBehaviour {
	VectorLine godDammit;
	VectorLine godDammit2;
	VectorLine goshDarn;
	VectorLine goshDarn2;
	public GameObject fcsRefresh;

	static public bool IsInside ( Collider test, Vector3 point)
	{
		Vector3    center;
		Vector3    direction;
		Ray        ray;
		RaycastHit hitInfo;
		bool       hit;
		
		// Use collider bounds to get the center of the collider. May be inaccurate
		// for some colliders (i.e. MeshCollider with a 'plane' mesh)
		center = test.bounds.center;
		//Debug.Log ("Center of target " + center + "Center of fcs " + point);
		// Cast a ray from point to center
		direction = center - point;

		ray = new Ray(point, direction);
		hit = test.Raycast(ray, out hitInfo, direction.magnitude);

		//Debug.Log ("hit: " + hit + " info: " + hitInfo.collider.tag);
		// If we hit the collider, point is outside. So we return !hit
		return !hit;
	}
	
	public CoreController targeter;
	// Use this for initialization
	void Start () {
		godDammit = VectorLine.SetLine3D (Color.green, new Vector3 (0.0f, 0.0f,1.0f), 
		                                  new Vector3 (1.0f, 1.0f, -1.0f), new Vector3(-1.0f,1.0f,-1.0f),
		                                  new Vector3 (0.0f, 0.0f,1.0f));
		godDammit2 = VectorLine.SetLine3D (Color.green, new Vector3 (0.0f, 0.0f,1.0f), 
		                                   new Vector3 (1.0f, -1.0f, -1.0f), new Vector3(-1.0f,-1.0f,-1.0f),
		                                   new Vector3 (0.0f, 0.0f,1.0f));
		goshDarn = VectorLine.SetLine3D (Color.green, new Vector3 (1.0f, 1.0f, -1.0f), new Vector3 (1.0f, -1.0f, -1.0f));
		goshDarn2 = VectorLine.SetLine3D (Color.green, new Vector3 (-1.0f, 1.0f, -1.0f), new Vector3 (-1.0f, -1.0f, -1.0f));

		goshDarn.drawTransform = transform;
		goshDarn2.drawTransform = transform;
		godDammit.drawTransform = transform;
		godDammit2.drawTransform = transform;
		Debug.Log ("FCS call refresh zeroth: " + fcsRefresh.activeSelf + " heirarchy: " + fcsRefresh.activeInHierarchy);
	}
	
	// Update is called once per frame
	void Update () {
		godDammit.Draw3D();
		godDammit2.Draw3D ();
		goshDarn.Draw3D ();
		goshDarn2.Draw3D ();
	}


	void OnTriggerEnter(Collider other){
		if (other.tag == "Target") {
			AddTarget(other);
				}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "Target") {
			if(!IsInside(this.collider, other.collider.bounds.center)){
				RemoveTarget(other);
			}
				}


		}

	public void AddTarget(Collider other){
		other.GetComponent<TargetLogic> ().EnableTarget ();
		targeter.AddTarget(other.gameObject);
	}

	public void RemoveTarget(Collider other) {
		other.GetComponent<TargetLogic> ().DisableTarget ();
		targeter.RemoveTarget (other.gameObject);
	}

	public void Refresh(){
		Debug.Log ("FCS call refresh first: " + fcsRefresh.activeSelf + " heirarchy: " + fcsRefresh.activeInHierarchy);
		fcsRefresh.SetActive (true);
		fcsRefresh.GetComponent<RescanBehavior> ().Reset ();
		Debug.Log ("FCS call refresh second: " + fcsRefresh.activeSelf + " heirarchy: " + fcsRefresh.activeInHierarchy);
	}
}
