﻿using UnityEngine;
using System.Collections;

public class MoveScene : MonoBehaviour {

	//This is the Vector used to define the bounds for the player. Code will use these values to adjust the physical space based 
	// on the player location. Main variable. As of writing, boundaries are manually defined, but planned to be overwritten using this 
	// Variable. The "center" of the scene will actually be defined at the front edge of the visible playing space in the center of
	// the bounds. The X value can go positive or negative to define distance from the center of the scene. But Y will be ground Level, and
	// will generally be positive. Z == 0 will be the foremost edge of the field and Z will always be positive as of writing.
	public Vector3 PlayerBounds;
	public float ScreenNudgeOffset;
	public GameObject CameraController; //Ie. Player
	// Use this for initialization
	void Start () {
		//PlayerBounds = new Vector3 (22.0f, 15.0f, 30.0f);
		ScreenNudgeOffset = 4.0f;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 playerRelative = CameraController.transform.position - transform.position;
		//Debug.Log ("Core pos: " + CameraController.transform.position + " Core \" Local \"  position: " + CameraController.transform.localPosition);
		if (Mathf.Abs (playerRelative.x) > PlayerBounds.x - ScreenNudgeOffset) {
			float nudgevalue = 0.0f;
			if(playerRelative.x > 0.0f){
				nudgevalue = playerRelative.x - (PlayerBounds.x - ScreenNudgeOffset);

			}
			else{
				nudgevalue = playerRelative.x + (PlayerBounds.x - ScreenNudgeOffset);

			}

			Vector3 newCameraPos = new Vector3(transform.position.x + nudgevalue, transform.position.y, transform.position.z);
			transform.position = newCameraPos;
				}
	}
}
