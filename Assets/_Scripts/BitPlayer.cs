
using UnityEngine;
using System.Collections;


		public class BitPlayer : MonoBehaviour
		{		
				public float health;
				public GameObject explosion;
				public GameObject targetCenter;
				public bool destructionflag = false;
				protected ArrayList target;
				
				protected void Start(){
					targetCenter = transform.FindChild ("TargetCenter").gameObject;
					target = new ArrayList ();

				}
				
				public void AddTarget(GameObject go){
					if (!target.Contains (go)) {
						target.Add (go);
					}
				}		


				public void RemoveTarget (GameObject go){
					//Debug.Log ("Target Removed " + go.name);
					target.Remove (go);
				}


				protected void Update(){
					if (destructionflag) {
					Destroy (gameObject);
					Instantiate(explosion,transform.position, transform.rotation);
					}

				}

				protected void OnCollisionEnter(Collision other){
				if (other.gameObject.tag == "Shot") {

				health = health - 10.0f;
				if(health < 0.0f){
					destructionflag = true;
					Projectile shot = other.gameObject.GetComponent<Projectile>();
					shot.getCore().RemoveTarget(targetCenter); //Unsure if this actually works
			}

		}
				}
		}


