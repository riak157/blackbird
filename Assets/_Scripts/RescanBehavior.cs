﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class RescanBehavior : MonoBehaviour {

	VectorLine outline1;
	VectorLine outline2;
	Vector3 [] Vertexes1;
	Vector3 [] Vertexes2;
	public float refreshTime;

	float startTime;
	// Use this for initialization
	void Start () {
	
		Vertexes1 = new Vector3[5];
		Vertexes1 [0] = new Vector3 (0.5f, 0.5f, 0.5f);
		Vertexes1 [1] = new Vector3(-0.5f, 0.5f, 0.5f);
		Vertexes1 [2] = new Vector3(-0.5f,-0.5f, 0.5f);
		Vertexes1 [3] = new Vector3 (0.5f,-0.5f, 0.5f);
		Vertexes1 [4] = new Vector3 (0.5f, 0.5f, 0.5f);

		Vertexes2 = (Vector3[]) Vertexes1.Clone ();

		for (int i = 0; i < Vertexes2.Length; i ++) {
			Vertexes2[i].z *= -1.0f;
				}


		outline1 = VectorLine.SetLine3D (Color.cyan, refreshTime, Vertexes1);
		outline2 = VectorLine.SetLine3D (Color.cyan, refreshTime, Vertexes2);

		outline1.drawTransform = transform;
		outline2.drawTransform = transform;

		transform.localPosition = new Vector3 (0.0f, 0.0f, 1.0f);
		startTime = Time.time;
	}


	public void Reset (){
		gameObject.SetActive (true);
		Vertexes1 = new Vector3[5];
		Vertexes1 [0] = new Vector3 (0.5f, 0.5f, 0.5f);
		Vertexes1 [1] = new Vector3(-0.5f, 0.5f, 0.5f);
		Vertexes1 [2] = new Vector3(-0.5f,-0.5f, 0.5f);
		Vertexes1 [3] = new Vector3 (0.5f,-0.5f, 0.5f);
		Vertexes1 [4] = new Vector3 (0.5f, 0.5f, 0.5f);
		
		Vertexes2 = (Vector3[]) Vertexes1.Clone ();
		
		for (int i = 0; i < Vertexes2.Length; i ++) {
			Vertexes2[i].z *= -1.0f;
		}
		
		
		outline1 = VectorLine.SetLine3D (Color.cyan, refreshTime, Vertexes1);
		outline2 = VectorLine.SetLine3D (Color.cyan, refreshTime, Vertexes2);
		
		outline1.drawTransform = transform;
		outline2.drawTransform = transform;
		startTime = Time.time;
		//Debug.Log ("Refresh Reset");

		}
	// Update is called once per frame
	void Update () {

		float lifeTime = Time.time - startTime;
		if (lifeTime < refreshTime) {
						float scale = lifeTime / refreshTime;
						//ScaleBoxes (scale);
						transform.localPosition = new Vector3 (0.0f, 0.0f, 1.0f - 2.0f * scale);
						transform.localScale = new Vector3 (2.0f * scale,2.0f * scale, 0.05f);
						outline1.Draw3D ();
						outline2.Draw3D ();
				} else {
			//Debug.Log("1: In rescan self: " + gameObject.activeSelf + " heirarchy: " + gameObject.activeInHierarchy);
			//outline1.StopDrawing3DAuto();
			//outline2.StopDrawing3DAuto();
			gameObject.SetActive(false);					
			//Debug.Log("2: In rescan self: " + gameObject.activeSelf + " heirarchy: " + gameObject.activeInHierarchy);
				}


	}


	public void OnTriggerEnter(Collider other){
		//Debug.Log ("trigger collsions " + other.tag);
		if (other.tag == "Target") {
			//Debug.Log("Collided with target");
						GetComponentInParent<FCSBehavior> ().AddTarget (other);
				}
		}

	void ScaleBoxes(float scale){
		if (scale == 0.0f) {
			scale = 0.01f;
				}
		for (int i = 0; i < Vertexes1.Length; i ++) {
			if(Vertexes1[i].x < 0.0f){ 
				Vertexes1[i].x = -scale;
				Vertexes2[i].x = -scale;
			}
			else{
				Vertexes1[i].x = scale;
				Vertexes2[i].x = scale;

			}

			if(Vertexes1[i].y < 0.0f){
				Vertexes1[i].y = -scale;
				Vertexes2[i].y = -scale;
			}
			else{
				Vertexes1[i].y = scale;
				Vertexes2[i].y = scale;

			}
		}

		outline1.Resize (Vertexes1);
		outline2.Resize (Vertexes2);

	}
}
