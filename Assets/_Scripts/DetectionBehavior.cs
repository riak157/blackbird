﻿using UnityEngine;
using System.Collections;

public class DetectionBehavior : MonoBehaviour {

	private BitPlayer detector;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RegisterDetector(BitPlayer bp){

		detector = bp;
	}

	void OnTriggerEnter(Collider other){
	
		if (other.tag == "Core") {

						detector.AddTarget (other.gameObject);
			Debug.Log ("detected");
				}
	}


	void OnTriggerExit(Collider other) {
		if (other.tag == "Core") {
						if (!FCSBehavior.IsInside (collider, other.bounds.center)) {
								detector.RemoveTarget (other.gameObject);
				Debug.Log ("undetected");
						}
				}
	}
}
