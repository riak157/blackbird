﻿using UnityEngine;
using System.Collections;
using Vectrosity;

public class GameController : MonoBehaviour {

	public GameObject physicalBoundries;
	Transform northWall;
	Transform eastWall;
	Transform southWall;
	Transform westWall;
	Transform ceiling;

	public GameObject enemy;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	void Start ()
	{	Physics.IgnoreLayerCollision (8, 9);
		Physics.IgnoreLayerCollision (8, 12);
		Physics.IgnoreLayerCollision (11, 12);
		Physics.IgnoreLayerCollision (12, 12);

		StartCoroutine (SpawnWaves ());
		northWall = physicalBoundries.transform.FindChild ("NorthWall");
		southWall = physicalBoundries.transform.FindChild ("SouthWall");
		eastWall = physicalBoundries.transform.FindChild ("EastWall");
		westWall = physicalBoundries.transform.FindChild ("WestWall");
		ceiling = physicalBoundries.transform.FindChild ("Ceiling");


	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++)
			{
				Vector3 spawnPosition = new Vector3 (eastWall.position.x + 2, Random.Range (2, ceiling.position.y - 4), 
				                                     Random.Range( southWall.position.z +2, northWall.position.z - 2));
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (enemy, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
